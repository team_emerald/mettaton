/*
 * http://ht-deko.com/arduino/tft_spi.html
 * 上記を参考にRESETピン追加
 * 
 * 20170808松尾
 * 
  */



#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include <URTouch.h>
#include <SD.h>
#include <MsTimer2.h>

//mp3
#include <DFPlayer_Mini_Mp3.h>


// For the Adafruit shield, these are the default.
#define TFT_RST 8 // 追加
#define TFT_DC 9
#define TFT_CS 10

#define SD_CS 7    //もともと4だった

#define BOXSIZE 26
#define PENRADIUS 3
int oldcolor, currentcolor;
int DesplaysizeX =320;
int DesplaysizeY =240;
int bsize =2 ; //ペンの太さ

long x, y;
//mp3
int val=0;
//UTFT    myGLCD(ILI9341_S5P,11,13,10,8,9);
URTouch  myTouch( 6, 5, 4, 3, 2);

// Use hardware SPI (on Uno, #13, #12, #11) and the above for CS/DC
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_RST);
// If using the breakout, change pins as desired
//Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_MOSI, TFT_CLK, TFT_RST, TFT_MISO);

void setup() {
  
  Serial.begin(9600);
  Serial.println("ILI9341 Test!"); 
 
  tft.begin();

  myTouch.InitTouch();
  myTouch.setPrecision(PREC_MEDIUM);
  //tft.setRotation(3);
  tft.setRotation(1);

    Serial.print("Initializing SD card...");
  if (!SD.begin(SD_CS)) 
  {
    Serial.println("failed!");
  }
  Serial.println("OK!");

  

  /*
  // read diagnostics (optional but can help debug problems)
  uint8_t x = tft.readcommand8(ILI9341_RDMODE);
  Serial.print("Display Power Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDMADCTL);
  Serial.print("MADCTL Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDPIXFMT);
  Serial.print("Pixel Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDIMGFMT);
  Serial.print("Image Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDSELFDIAG);
  Serial.print("Self Diagnostic: 0x"); Serial.println(x, HEX); 
  
  Serial.println(F("Benchmark                Time (microseconds)"));
  delay(10);

  */

  tft.fillScreen(ILI9341_BLACK);
  unsigned long start = micros();
  //bmpDraw("M1.bmp",0,0);


  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(4);


  // select the current color 'red'
  currentcolor = ILI9341_RED;
  mp3_set_serial (Serial);  //set softwareSerial for DFPlayer-mini mp3 module 
  delay(1);  //wait 1ms for mp3 module to set volume
  mp3_set_volume (20);
  MsTimer2::set(200, timerFire);
  MsTimer2::start();


  while (myTouch.dataAvailable() == false) 
  {
               //高速メタトン顔変化
               int FacePattern[]={1,2,2,2,1,
                                  1,1,2,1,1,
                                  1,2,1,2,1,
                                  1,2,2,2,1};
               MTTFaceChange(FacePattern);
               delay(50);
               
               int FacePattern2[]={2,1,1,1,2,
                                  2,2,1,2,2,
                                  2,1,2,1,2,
                                  2,1,1,1,2};   
               MTTFaceChange(FacePattern2); 
               delay(50);           
  }           
  bmpDraw("drawmode.bmp",0,0);
    tft.setCursor(230, 14);
    tft.println("-");
    tft.setCursor(260, 14);
    tft.println("+");
  

}


void loop(void) {

  while (myTouch.dataAvailable() == true)
  {
    myTouch.read();
    x = 320-myTouch.getX();
    y = 240-myTouch.getY();

    //tft.setCursor(50, 50);
    //tft.println(myTouch.TP_X);
    //tft.setCursor(100, 100);
    //tft.println(myTouch.TP_Y);
    if ((x!=-1) and (y!=-1))
    {
      if ((x>18) and (x<301) and (y>35) and (y<204))///枠内ならお絵かき
      {

          tft.fillRect(x, y, bsize, bsize, currentcolor);
      }
      else///枠外
      {
       if((y>216) and (y<236))///button
        {
          if((x>16) and (x<70))///erase mode
          {
             currentcolor = ILI9341_BLACK;
          }
          else if((x>92) and (x<146))///
          {
               tft.setCursor(50, 50);
               tft.println("S");
               tft.setCursor(50, 80);
               tft.println("A");
               tft.setCursor(50, 110);
               tft.println("V");
               tft.setCursor(50, 140);
               tft.println("E");
               
               delay(100);


               //高速メタトン顔変化
               tft.fillScreen(ILI9341_BLACK);
               for(int m=0; m<4; m+=1)
               {
                  tft.fillRect(4, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(68, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(132, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(196, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(260, 4+m*60, 56, 52, currentcolor);
               }
               currentcolor = ILI9341_YELLOW;
               for(int m=0; m<4; m+=1)
               {
                  tft.fillRect(4, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(68, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(132, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(196, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(260, 4+m*60, 56, 52, currentcolor);
               }
               currentcolor = ILI9341_RED;
               for(int m=0; m<4; m+=1)
               {
                  tft.fillRect(4, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(68, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(132, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(196, 4+m*60, 56, 52, currentcolor);
                  tft.fillRect(260, 4+m*60, 56, 52, currentcolor);
               }
          }
          else if((x>172) and (x<226))///
          {
               bmpDraw("drawmode.bmp",0,0);
          }
          else if((x>250) and (x<304))///
          {
               bmpDraw("haphome.bmp",0,0);
               bmpDraw("mttshop.bmp",0,0);
               bmpDraw("naphome.bmp",0,0);
               bmpDraw("undyne.bmp",0,0);
          }
          
       }

        else if((y<29) and (x>230)) //筆替え　太さ
        {
          if((x>260))
          {
             bsize = bsize+1;         
             delay(50);
          }
          else if((x>230))
          {
             bsize = bsize-1;
              if(bsize <1)
             {
             bsize =1;
            }
             delay(50);
          }
        }

        else if((y<29)) //筆替え　iro
        {
          int colorwidth =25;
          if((x>30+colorwidth*6))
          {
             currentcolor = ILI9341_MAGENTA;
          }
         else if((x>30+colorwidth*5))
          {
             currentcolor = ILI9341_BLUE;
          } 
          else if((x>30+colorwidth*4))
          {
             currentcolor = ILI9341_CYAN;
          }
          else if((x>30+colorwidth*3))
          {
             currentcolor = ILI9341_DARKGREEN;
          }
          else if((x>30+colorwidth*2))
          {
             currentcolor = ILI9341_YELLOW;
          }
          else if((x>30+colorwidth*1))
          {
             currentcolor = ILI9341_ORANGE;
          }
          else if((x>30+colorwidth*0))
          {
             currentcolor = ILI9341_RED;
         }         
        }

        tft.fillRect(286,2,24, 13, ILI9341_BLACK);
        tft.fillRect(298, 7, bsize, bsize, currentcolor);
      }
      
    }
  }
}

unsigned long testFillScreen() {
  unsigned long start = micros();
  tft.fillScreen(ILI9341_BLACK);
  yield();
  tft.fillScreen(ILI9341_RED);
  yield();
  tft.fillScreen(ILI9341_GREEN);
  yield();
  tft.fillScreen(ILI9341_BLUE);
  yield();
  tft.fillScreen(ILI9341_BLACK);
  yield();
  return micros() - start;
}

unsigned long testText() {
  tft.fillScreen(ILI9341_BLACK);
  unsigned long start = micros();
  tft.setCursor(0, 0);
  tft.setTextColor(ILI9341_WHITE);  tft.setTextSize(1);
  tft.println("Hello World!");
  tft.setTextColor(ILI9341_YELLOW); tft.setTextSize(2);
  tft.println(1234.56);
  tft.setTextColor(ILI9341_RED);    tft.setTextSize(3);
  tft.println(0xDEADBEEF, HEX);
  tft.println();
  tft.setTextColor(ILI9341_GREEN);
  tft.setTextSize(5);
  tft.println("Groop");
  tft.setTextSize(2);
  tft.println("I implore thee,");
  tft.setTextSize(1);
  tft.println("my foonting turlingdromes.");
  tft.println("And hooptiously drangle me");
  tft.println("with crinkly bindlewurdles,");
  tft.println("Or I will rend thee");
  tft.println("in the gobberwarts");
  tft.println("with my blurglecruncheon,");
  tft.println("see if I don't!");
  return micros() - start;
}

unsigned long testLines(uint16_t color) {
  unsigned long start, t;
  int           x1, y1, x2, y2,
                w = tft.width(),
                h = tft.height();

  tft.fillScreen(ILI9341_BLACK);
  yield();
  
  x1 = y1 = 0;
  y2    = h - 1;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = w - 1;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t     = micros() - start; // fillScreen doesn't count against timing

  yield();
  tft.fillScreen(ILI9341_BLACK);
  yield();

  x1    = w - 1;
  y1    = 0;
  y2    = h - 1;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = 0;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t    += micros() - start;

  yield();
  tft.fillScreen(ILI9341_BLACK);
  yield();

  x1    = 0;
  y1    = h - 1;
  y2    = 0;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = w - 1;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
  t    += micros() - start;

  yield();
  tft.fillScreen(ILI9341_BLACK);
  yield();

  x1    = w - 1;
  y1    = h - 1;
  y2    = 0;
  start = micros();
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = 0;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);

  yield();
  return micros() - start;
}

unsigned long testFastLines(uint16_t color1, uint16_t color2) {
  unsigned long start;
  int           x, y, w = tft.width(), h = tft.height();

  tft.fillScreen(ILI9341_BLACK);
  start = micros();
  for(y=0; y<h; y+=5) tft.drawFastHLine(0, y, w, color1);
  for(x=0; x<w; x+=5) tft.drawFastVLine(x, 0, h, color2);

  return micros() - start;
}

unsigned long testRects(uint16_t color) {
  unsigned long start;
  int           n, i, i2,
                cx = tft.width()  / 2,
                cy = tft.height() / 2;

  tft.fillScreen(ILI9341_BLACK);
  n     = min(tft.width(), tft.height());
  start = micros();
  for(i=2; i<n; i+=6) {
    i2 = i / 2;
    tft.drawRect(cx-i2, cy-i2, i, i, color);
  }

  return micros() - start;
}

unsigned long testFilledRects(uint16_t color1, uint16_t color2) {
  unsigned long start, t = 0;
  int           n, i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  n = min(tft.width(), tft.height());
  for(i=n; i>0; i-=6) {
    i2    = i / 2;
    start = micros();
    tft.fillRect(cx-i2, cy-i2, i, i, color1);
    t    += micros() - start;
    // Outlines are not included in timing results
    tft.drawRect(cx-i2, cy-i2, i, i, color2);
    yield();
  }

  return t;
}

unsigned long testFilledCircles(uint8_t radius, uint16_t color) {
  unsigned long start;
  int x, y, w = tft.width(), h = tft.height(), r2 = radius * 2;

  tft.fillScreen(ILI9341_BLACK);
  start = micros();
  for(x=radius; x<w; x+=r2) {
    for(y=radius; y<h; y+=r2) {
      tft.fillCircle(x, y, radius, color);
    }
  }

  return micros() - start;
}

unsigned long testCircles(uint8_t radius, uint16_t color) {
  unsigned long start;
  int           x, y, r2 = radius * 2,
                w = tft.width()  + radius,
                h = tft.height() + radius;

  // Screen is not cleared for this one -- this is
  // intentional and does not affect the reported time.
  start = micros();
  for(x=0; x<w; x+=r2) {
    for(y=0; y<h; y+=r2) {
      tft.drawCircle(x, y, radius, color);
    }
  }

  return micros() - start;
}

unsigned long testTriangles() {
  unsigned long start;
  int           n, i, cx = tft.width()  / 2 - 1,
                      cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  n     = min(cx, cy);
  start = micros();
  for(i=0; i<n; i+=5) {
    tft.drawTriangle(
      cx    , cy - i, // peak
      cx - i, cy + i, // bottom left
      cx + i, cy + i, // bottom right
      tft.color565(i, i, i));
  }

  return micros() - start;
}

unsigned long testFilledTriangles() {
  unsigned long start, t = 0;
  int           i, cx = tft.width()  / 2 - 1,
                   cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  start = micros();
  for(i=min(cx,cy); i>10; i-=5) {
    start = micros();
    tft.fillTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(0, i*10, i*10));
    t += micros() - start;
    tft.drawTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(i*10, i*10, 0));
    yield();
  }

  return t;
}

unsigned long testRoundRects() {
  unsigned long start;
  int           w, i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  w     = min(tft.width(), tft.height());
  start = micros();
  for(i=0; i<w; i+=6) {
    i2 = i / 2;
    tft.drawRoundRect(cx-i2, cy-i2, i, i, i/8, tft.color565(i, 0, 0));
  }

  return micros() - start;
}

unsigned long testFilledRoundRects() {
  unsigned long start;
  int           i, i2,
                cx = tft.width()  / 2 - 1,
                cy = tft.height() / 2 - 1;

  tft.fillScreen(ILI9341_BLACK);
  start = micros();
  for(i=min(tft.width(), tft.height()); i>20; i-=6) {
    i2 = i / 2;
    tft.fillRoundRect(cx-i2, cy-i2, i, i, i/8, tft.color565(0, i, 0));
    yield();
  }

  return micros() - start;
}



// This function opens a Windows Bitmap (BMP) file and
// displays it at the given coordinates.  It's sped up
// by reading many pixels worth of data at a time
// (rather than pixel by pixel).  Increasing the buffer
// size takes more of the Arduino's precious RAM but
// makes loading a little faster.  20 pixels seems a
// good balance.

#define BUFFPIXEL 20

void bmpDraw(char *filename, int16_t x, int16_t y) {

  File     bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3*BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col, x2, y2, bx1, by1;
  uint8_t  r, g, b;
  uint32_t pos = 0, startTime = millis();

  if((x >= tft.width()) || (y >= tft.height())) return;

  Serial.println();
  Serial.print(F("Loading image '"));
  Serial.print(filename);
  Serial.println('\'');

  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    Serial.print(F("File not found"));
    return;
  }

  // Parse BMP header
  if(read16(bmpFile) == 0x4D42) { // BMP signature
    Serial.print(F("File size: ")); Serial.println(read32(bmpFile));
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data
    Serial.print(F("Image Offset: ")); Serial.println(bmpImageoffset, DEC);
    // Read DIB header
    Serial.print(F("Header size: ")); Serial.println(read32(bmpFile));
    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);
    if(read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      Serial.print(F("Bit Depth: ")); Serial.println(bmpDepth);
      if((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!
        Serial.print(F("Image size: "));
        Serial.print(bmpWidth);
        Serial.print('x');
        Serial.println(bmpHeight);

        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;

        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if(bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }

        // Crop area to be loaded
        x2 = x + bmpWidth  - 1; // Lower-right corner
        y2 = y + bmpHeight - 1;
        if((x2 >= 0) && (y2 >= 0)) { // On screen?
          w = bmpWidth; // Width/height of section to load/display
          h = bmpHeight;
          bx1 = by1 = 0; // UL coordinate in BMP file
          if(x < 0) { // Clip left
            bx1 = -x;
            x   = 0;
            w   = x2 + 1;
          }
          if(y < 0) { // Clip top
            by1 = -y;
            y   = 0;
            h   = y2 + 1;
          }
          if(x2 >= tft.width())  w = tft.width()  - x; // Clip right
          if(y2 >= tft.height()) h = tft.height() - y; // Clip bottom
  
          // Set TFT address window to clipped image bounds
          tft.startWrite(); // Requires start/end transaction now
          tft.setAddrWindow(x, y, w, h);
  
          for (row=0; row<h; row++) { // For each scanline...
  
            // Seek to start of scan line.  It might seem labor-
            // intensive to be doing this on every line, but this
            // method covers a lot of gritty details like cropping
            // and scanline padding.  Also, the seek only takes
            // place if the file position actually needs to change
            // (avoids a lot of cluster math in SD library).
            if(flip) // Bitmap is stored bottom-to-top order (normal BMP)
              pos = bmpImageoffset + (bmpHeight - 1 - (row + by1)) * rowSize;
            else     // Bitmap is stored top-to-bottom
              pos = bmpImageoffset + (row + by1) * rowSize;
            pos += bx1 * 3; // Factor in starting column (bx1)
            if(bmpFile.position() != pos) { // Need seek?
              tft.endWrite(); // End TFT transaction
              bmpFile.seek(pos);
              buffidx = sizeof(sdbuffer); // Force buffer reload
              tft.startWrite(); // Start new TFT transaction
            }
            for (col=0; col<w; col++) { // For each pixel...
              // Time to read more pixel data?
              if (buffidx >= sizeof(sdbuffer)) { // Indeed
                tft.endWrite(); // End TFT transaction
                bmpFile.read(sdbuffer, sizeof(sdbuffer));
                buffidx = 0; // Set index to beginning
                tft.startWrite(); // Start new TFT transaction
              }
              // Convert pixel from BMP to TFT format, push to display
              b = sdbuffer[buffidx++];
              g = sdbuffer[buffidx++];
              r = sdbuffer[buffidx++];
              tft.writePixel(tft.color565(r,g,b));
            } // end pixel
          } // end scanline
          tft.endWrite(); // End last TFT transaction
        } // end onscreen
        Serial.print(F("Loaded in "));
        Serial.print(millis() - startTime);
        Serial.println(" ms");
      } // end goodBmp
    }
  }

  bmpFile.close();
  if(!goodBmp) Serial.println(F("BMP format not recognized."));
}

// These read 16- and 32-bit types from the SD card file.
// BMP data is stored little-endian, Arduino is little-endian too.
// May need to reverse subscript order if porting elsewhere.

uint16_t read16(File &f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}

uint32_t read32(File &f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}


void timerFire(){
  val = analogRead(5); 
  if (val==0){
    mp3_play (6);
    delay(200);
    }
}

void MTTFaceChange(int tempface[20]){
               for(int j=0; j<4; j+=1)
               {
                for(int i=0; i<5; i+=1)
                {
                  //change color
                  switch (tempface[i+j*5]) 
                  {
                    case 1:
                       currentcolor = ILI9341_RED;
                       break;
                    case 2:
                       currentcolor = ILI9341_YELLOW;
                       break;
                    default:
                       currentcolor = ILI9341_BLACK;
                       // 
                   }
                  tft.fillRect(4+i*64, 4+j*60, 56, 52, currentcolor);
                }
               }

}
