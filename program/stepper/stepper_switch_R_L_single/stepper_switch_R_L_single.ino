#include <MsTimer2.h>
// Define pins;
#define ENABLE_PIN 0
#define RESET_PIN 1
#define CLK_1 2
#define CW_1 3
#define CLK_2 4
#define CW_2 5

//stepper setup
bool pwm = HIGH;
void pulse(void);

int front_right_val;
int front_left_val;
int back_right_val;
int back_left_val;

void setup() {
  Serial.begin(9600);

   for (int i = 0 ; i <= 5 ; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
   }
   pinMode(13,INPUT);
   pinMode(12,INPUT);
   pinMode(11,OUTPUT);
   pinMode(10,INPUT); 
   pinMode(9,INPUT);   
  
   digitalWrite(13, HIGH);
   digitalWrite(12, HIGH);
   digitalWrite(11, LOW);
   digitalWrite(10, HIGH);
   digitalWrite(9, HIGH);


   digitalWrite(RESET_PIN, HIGH);
   digitalWrite(ENABLE_PIN, HIGH);
   
//速度の決定
   MsTimer2::set(5, pulse); 
   MsTimer2::start(); 
   digitalWrite(CW_1, HIGH);
   digitalWrite(CW_2, LOW);
}

void loop() {
 front_right_val = digitalRead(9);
 back_right_val = digitalRead(10);
 front_left_val = digitalRead(12);
 back_left_val= digitalRead(13);

//Serial.print(right_val);
//Serial.println(left_val);

if (front_right_val==0&&back_right_val==0){
      digitalWrite(CW_1, HIGH);
      digitalWrite(CW_2, LOW);
      //MsTimer2::start(); 
  }
else if(front_left_val==0&&back_left_val==0){
      digitalWrite(CW_1, LOW);
      digitalWrite(CW_2, HIGH);  
     // MsTimer2::start(); 
  }
else if(front_left_val==0&&back_right_val==0){
      digitalWrite(CW_1, LOW);
      digitalWrite(CW_2, LOW);  
     // MsTimer2::start(); 
  }
else if(front_right_val==0&&back_left_val==0){
      digitalWrite(CW_1, HIGH);
      digitalWrite(CW_2, HIGH);  
     // MsTimer2::start(); 
  }
else{
    MsTimer2::stop(); 
    delay(500);
    MsTimer2::start(); 
  }
}

void pulse(void){
  if (pwm == LOW) {
    pwm = HIGH;
  }
  else{
    pwm=LOW;
    }
    digitalWrite(CLK_1,pwm);
    digitalWrite(CLK_2,pwm);
  }
