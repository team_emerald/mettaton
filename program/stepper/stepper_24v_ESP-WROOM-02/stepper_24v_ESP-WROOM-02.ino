#include <MsTimer2.h>
#include "ESP8266.h"
#include <SoftwareSerial.h>
// Define pins;
#define CW_1 5
#define CLK_1 6
#define CW_2 7
#define CLK_2 8
#define RESET_PIN 9
#define ENABLE_PIN 10

//stepper setup
bool pwm = HIGH;
void pulse(void);

// Wi-Fi SSID
#define SSID        "HUMAX-DC43D"
// Wi-Fi PASSWORD
#define PASSWORD    "NmTjNmFkNWNhL"
// サーバーのホスト名
#define HOST_NAME   "curiouser.sakura.ne.jp"
// ポート番号
#define HOST_PORT   80
 
SoftwareSerial mySerial(2, 3); /* RX, TX */
ESP8266 wifi(mySerial);



/**
 * 初期設定
 */
void setup(void)
{
  // デジタル13番ピンを出力として設定
  //pinMode(4, OUTPUT);
  //stepperの速度設定
  
  Serial.begin(9600);
  Serial.print("setup begin\r\n");

  Serial.print("FW Version:");
  Serial.println(wifi.getVersion().c_str());

  if (wifi.setOprToStationSoftAP()) {
    Serial.print("to station + softap ok\r\n");
  } else {
    Serial.print("to station + softap err\r\n");
  }

  if (wifi.joinAP(SSID, PASSWORD)) {
    Serial.print("Join AP success\r\n");
    Serial.print("IP:");
    Serial.println( wifi.getLocalIP().c_str());
  } else {
    Serial.print("Join AP failure\r\n");
  }

  if (wifi.disableMUX()) {
    Serial.print("single ok\r\n");
  } else {
    Serial.print("single err\r\n");
  }
 
  Serial.print("setup end\r\n");

   for (int i = 5 ; i <= 11 ; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
    }

// This is the order according to the application notes
  digitalWrite(RESET_PIN, HIGH);
  digitalWrite(ENABLE_PIN, HIGH);
//速度の決定
  MsTimer2::set(5, pulse); 
  MsTimer2::start(); 
}

/**
 * ループ処理
 */
void loop(void)
{
  uint8_t buffer[512] = {0};

  // TCPで接続
  if (wifi.createTCP(HOST_NAME, HOST_PORT)) {
    Serial.print("create tcp ok\r\n");
  } else {
    Serial.print("create tcp err\r\n");
  }

  // サーバーへ渡す情報
  // 例 http://test.co.jp/arduino-esp/ledGet.php
  //char *sendStr = "GET /~yamamoton/ledGet.php HTTP/1.1\r\nHost: mizuuchi.lab.tuat.ac.jp\r\n User-Agent: arduino\r\n\r\n";
  //char *sendStr = "GET /ledGet.php HTTP/1.0\r\n User-Agent: arduino\r\n\r\n";
  //char *sendStr = "GET /ledGet.php HTTP/1.0\r\nHost: mizuuchi.lab.tuat.ac.jp\r\nUser-Agent: arduino\r\n\r\n";
  char *sendStr = "GET /mettatondirGet.php?=$line HTTP/1.0\r\nHost: curiouser.sakura.ne.jp\r\nUser-Agent: arduino\r\n\r\n";

  wifi.send((const uint8_t*)sendStr, strlen(sendStr));

  //サーバからの文字列を入れるための変数
  String resultCode = "";

  // 取得した文字列の長さ
  uint32_t len = wifi.recv(buffer, sizeof(buffer), 10000);
  Serial.print(len);

//if (len > 0) {
// Serial.print("Received:[");
// for(uint32_t i = 0; i < len; i++) {
// Serial.print((char)buffer[i]);
// }
// Serial.print("]\r\n");
// }


  // 取得した文字数が0でなければ
  if (len > 0) {
    for(uint32_t i = 0; i < len; i++) {
      resultCode += (char)buffer[i];
      Serial.print((char)buffer[i]);
    }

    // lastIndexOfでresultCodeの最後から改行を探す
    int lastLF = resultCode.lastIndexOf('\n');

    // resultCodeの長さを求める
    int resultCodeLength = resultCode.length();
  
    // substringで改行コードの次の文字から最後までを求める
    String resultString = resultCode.substring(lastLF+1, resultCodeLength);

    // 前後のスペースを取り除く
    resultString.trim();
    Serial.print(resultString);
    // 取得した文字列がONならば
    if(resultString == "Left") {
      digitalWrite(4, HIGH);
      MsTimer2::start(); 
      digitalWrite(CW_1, LOW);
      digitalWrite(CW_2, HIGH);
            
      //Serial.print("high\r\n");
    } else if(resultString == "Right"){
      digitalWrite(4, LOW);
      MsTimer2::start(); 
      digitalWrite(CW_1, HIGH);
      digitalWrite(CW_2, LOW);

      //Serial.print("low\r\n");
    }
    else{
            MsTimer2::stop(); 
//        digitalWrite(4, LOW);
 //         delay(100);
//        digitalWrite(4, HIGH);
//        delay(100);
//        digitalWrite(4, LOW);
//        delay(100);
//        digitalWrite(4, HIGH);
//        delay(500);
      }
      //delay(100);

  }

  // 200ミリ秒待つ
  delay(50);
}

void pulse(void){
  if (pwm == LOW) {
    pwm = HIGH;
  }
  else{
    pwm=LOW;
    }
    digitalWrite(CLK_1,pwm);
    digitalWrite(CLK_2,pwm);
  }
