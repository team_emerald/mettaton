// Define pins;
#define CW_1 3
#define CLK_1 4
#define CW_2 6
#define CLK_2 7
#define RESET_PIN 9
#define ENABLE_PIN 10
 
void setup() {
// Make all the pins OUTPUT and set them to LOW
for (int i = 0 ; i <= 11 ; i++) {
pinMode(i, OUTPUT);
digitalWrite(i, LOW);
}
 
// This is the order according to the application notes
digitalWrite(RESET_PIN, HIGH);
digitalWrite(ENABLE_PIN, HIGH);

Serial.begin(2000000);

}
 
int counter = 0;
int pwm = LOW;
int counter_cw=0;
int dir_1=LOW;
int dir_2=HIGH;
unsigned long time1;


void loop() {
// Send out a clock signal. switch the direction bit every 2000 clocks.

digitalWrite(CW_1, dir_1);
digitalWrite(CW_2, dir_2);

digitalWrite(CLK_1, pwm);
digitalWrite(CLK_2, pwm);

counter++;
counter_cw++;
//Serial.print(counter_cw);
//Serial.print("\n");


//パルス切り替え
if (counter == 200) {
if (pwm == LOW) {
pwm = HIGH;
time1= micros();;
//Serial.print("us =");
//Serial.print(time1);
//Serial.print("\n");
}
else {
pwm = LOW;
time1= micros();;
//Serial.print("us =");
//Serial.print(time1);
//Serial.print("\n");
}

counter=0;
}

//方向切り替え
if(counter_cw==25000){
Serial.print("change");
Serial.print("\n");

if(dir_1==LOW){
  dir_1=HIGH;
  dir_2=LOW;
  }
else{
  dir_1=LOW;
  dir_2=HIGH;  
  }
counter_cw=0;

}
}
