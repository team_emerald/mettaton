#include <SD.h>
const int chipSelect = 10;
const int pinRecordSW = 5;
const int pinPlaySW  = 4;
const int pinPlaySound = 3;

int stateRecordSW;
int countRecordSW;
int statePlaySW;
int countPlaySW;
boolean isRecordMode;
boolean isPlayMode;
File dataFile;


void setup(){
  Serial.begin(115200);

  pinMode(pinPlaySound, OUTPUT);
  TCCR2A = _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
  TCCR2B = _BV(CS20);

  pinMode(SS, OUTPUT);
  SD.begin(chipSelect);

  //pinMode(pinRecordSW,  INPUT_PULLUP);
  pinMode(pinRecordSW,  INPUT_PULLUP);
  pinMode(pinPlaySW,  INPUT_PULLUP);
  stateRecordSW = 1;
  countRecordSW = 0;
  isRecordMode = false;
  statePlaySW = 1;
  countPlaySW = 0;
  isPlayMode = false;
  Serial.println("start");
}

void loop(){
  readPIN();

  controlRecordMode();
  if (isRecordMode){
    recordValue();
    //delay(1000);
  }

  controlPlayMode();
  if (isPlayMode){
    playValue();
  }  
}

//##########################################################
//##########################################################
//##########################################################

void controlRecordMode(){
  if (stateRecordSW == 0){
    countRecordSW++;
    if (countRecordSW == 10){

      if (!isRecordMode){
        if (isPlayMode){
          isPlayMode = false;
          closeSD();
        }
        isRecordMode = true;
        openSDToRecord();
        if (!dataFile) {
          openSDToRecord();
        }
      } else if (isRecordMode){
        isRecordMode = false;
        closeSD();
      }

      delay(1000);
    }
  } else {
    countRecordSW = 0;
  }
}

void controlPlayMode(){
  if (statePlaySW == 0){
    countPlaySW++;
    if (countPlaySW == 10){

      if (!isPlayMode){
        if (isRecordMode){
          isRecordMode = false;
          closeSD();
        }
        isPlayMode = true;
        openSDToPlay();
        if (!dataFile) {
          openSDToPlay();
        }
      } else if (isPlayMode){
        isPlayMode = false;
        closeSD();
      }

      delay(1000);
    }
  } else {
    countPlaySW = 0;
  }
}

void readPIN(){
  stateRecordSW = digitalRead(pinRecordSW);
  statePlaySW  = digitalRead(pinPlaySW);
  
}

void openSDToRecord(){
  SD.remove("data.txt");
  dataFile = SD.open("data.txt", FILE_WRITE);
  Serial.println("SD was opened to Record");
}

void openSDToPlay(){
  dataFile = SD.open("data.txt");
  Serial.println("SD was opened to Play");
}

void recordValue(){
  //int value = analogRead(0);
  int valuePre = analogRead(0);
  int value = int(float(valuePre)/4.0);
  //dataFile.write(value);
  //Serial.print("Record:");
  Serial.println(valuePre);
}

void playValue(){
  while (dataFile.available()) {
    int value = dataFile.read();
    OCR2B = value;
    //delayMicroseconds(150);
    delayMicroseconds(150);
    Serial.println(value);
  }
  isPlayMode = false;
  closeSD();
}

void closeSD(){
  if (dataFile) {
    dataFile.close();
    Serial.println("SD was closed");
  }
  OCR2B = 0;
}
