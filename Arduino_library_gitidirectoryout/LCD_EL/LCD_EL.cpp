#include "Arduino.h"
#include "LCD_EL.h"

LCD_EL::LCD_EL(int rs, int rw, int enable,int db0, int db1, int db2,int db3, int db4, int db5, int db6, int db7)
{
  rs=_RS;
  rw=_RW;
  enable=_Enable;

  _DB0 = db0;
  _DB1 = db1;
  _DB2 = db2;
  _DB3 = db3;
  _DB4 = db4;
  _DB5 = db5;
  _DB6 = db6;
  _DB7 = db7;

}

void LCD_EL::LCD_test(int tosi){
  pinMode(13,OUTPUT);
  digitalWrite(13,tosi);
  delay(500);
  digitalWrite(13,LOW);
  delay(500);
  digitalWrite(13,tosi);
  }

void LCD_EL::LCD_cmd_set(int data7, int data6, int data5, int data4, int data3, int data2, int data1, int data0){

 digitalWrite(_DB0,data0);
 digitalWrite(_DB1,data1);
 digitalWrite(_DB2,data2);
 digitalWrite(_DB3,data3);
 digitalWrite(_DB4,data4);
 digitalWrite(_DB5,data5);
 digitalWrite(_DB6,data6);
 digitalWrite(_DB7,data7);

 delayMicroseconds(50);
 digitalWrite(_Enable, 1);
 delayMicroseconds(50);    // Timing Spec?
 digitalWrite(_Enable, 0);
 }

int LCD_EL::LCD_check_busy(){
  int val;
  digitalWrite(_RW,1);
  val=digitalRead(_DB7);
  return val;
}

void LCD_EL::LCD_set_pin(){
  pinMode(_RS, OUTPUT);
  pinMode(_RW, OUTPUT);
  pinMode(_Enable, OUTPUT);

 digitalWrite(_RS,LOW);
 digitalWrite(_Enable,LOW);
 digitalWrite(_RW,LOW);

 delayMicroseconds(50000);

 pinMode(_DB0, OUTPUT);
 pinMode(_DB1, OUTPUT);
 pinMode(_DB2, OUTPUT);
 pinMode(_DB3, OUTPUT);
 pinMode(_DB4, OUTPUT);
 pinMode(_DB5, OUTPUT);
 pinMode(_DB6, OUTPUT);
 pinMode(_DB7, OUTPUT);

}

void LCD_EL::LCD_init(){
LCD_set_pin();
LCD_write4bits(0b0011);
delay(5);
LCD_write4bits(0b0011);
delayMicroseconds(100);
LCD_write4bits(0b0011);
delayMicroseconds(40);

LCD_write4bits(0b0010);
delayMicroseconds(40);
LCD_write8bits(0b00101000);
delayMicroseconds(40);
LCD_write8bits(0b00001000);
delayMicroseconds(40);
LCD_write8bits(0b00000001);
delayMicroseconds(40);
LCD_write8bits(0b00000110);

LCD_write8bits(0b00001000); //display off
delay(2);
LCD_write8bits(0b00001100); //display on
delay(2);
LCD_write8bits(0b00000110); //entry mode set
delay(2);
}

void LCD_EL::LCD_write4bits(uint8_t value)
{
    digitalWrite(_DB4, (value >> 0) & 0x01);
    digitalWrite(_DB5, (value >> 1) & 0x01);
    digitalWrite(_DB6, (value >> 2) & 0x01);
    digitalWrite(_DB7, (value >> 3) & 0x01);

  delayMicroseconds(50); // Timing spec?
  digitalWrite(_Enable, 1);
  delayMicroseconds(50);    // Timing Spec?
  digitalWrite(_Enable, 0);
}

void LCD_EL::LCD_write8bits(uint8_t chara){
  LCD_write4bits((chara >> 4) & 0b00001111);
  Serial.println((chara >> 4) & 0b00001111);
  LCD_write4bits(chara & 0b00001111);
}

void LCD_EL::LCD_Graphic_SetXaddr(uint8_t addr){
	LCD_write8bits(0b10000000 | addr);
}

void LCD_EL::LCD_Graphic_SetYaddr(uint8_t addr2){
	LCD_write8bits(0b01000000 | addr2);
}


void LCD_EL::LCD_Graphic_SendFullData(const uint8_t* dataArray){
	for(y=0; y<2; y++){
		for(x=0; x<100; x++){
			LCD_Graphic_SetXaddr(x);
			LCD_Graphic_SetYaddr(y);
      digitalWrite(_RS,1);
			LCD_write4bits(dataArray[y*100+x]);
      digitalWrite(_RS,0);
		}
	}
}


void LCD_EL::LCD_clear(){
LCD_write8bits(0b00000001);
}
