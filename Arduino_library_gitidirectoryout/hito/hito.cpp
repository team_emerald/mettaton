#include "hito.h"    //作成したクラスヘッダファイルを読み込む
#include <string.h>

hito::hito()
{
    strcpy(name,"(未定義)");
    age=0;
}

hito::hito(char *ss)
{
    strcpy(name,ss);
    age=0;
}

hito::hito(int tosi)
{
    strcpy(name,"(未定義)");
    if(tosi<0) age=0; else age=tosi;
}

hito::hito(char *ss, int tosi)
{
    strcpy(name,ss);
    if(tosi<0) age=0; else age=tosi;
}

void hito::disp()
{
//    cout << "名前：" << name << endl;
//    cout << "年齢：" << age << endl;
}
