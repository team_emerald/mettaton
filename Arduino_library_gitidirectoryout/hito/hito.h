#ifndef _INC_HITO    //まだ読み込まれていなければ以下の処理をする
#define _INC_HITO    //すでに読み込まれているという目印をつける

#include <string.h>

class hito
{
    char name[40];
    int age;
public:
    hito();
    hito(char* ss);
    hito(int tosi);
    hito(char *ss, int tosi);
    void setname(char* ss) { strcpy(name,ss); }
    void setage(int tosi) { age=(tosi<0) ? 0: tosi; }
    char* getname() { return name; }
    int getage() { return age; }
    void disp();
};
#endif    //INC_HITO
