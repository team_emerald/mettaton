#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

void setup () {
    Serial.begin (9600);
    mp3_set_serial (Serial1);      //set Serial for DFPlayer-mini mp3 module 
    delay(20);                     // delay 1ms to set volume
    mp3_set_volume (20);          // value 0~30
    pinMode(12, OUTPUT);

}

void loop () { 
    digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)       
    mp3_random_play (); 
    delay (100000);
    digitalWrite(12, LOW);   // turn the LED on (HIGH is the voltage level)       

}
