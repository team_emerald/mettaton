#include <MsTimer2.h>
#include <Servo.h>
#include "DFRobotDFPlayerMini.h"
#include "SoftwareSerial.h"

//pin
/*LED*/
#define yellow_control_pin 6
#define red_control_pin 8

/*servo*/
#define front_servo_control_pin 11
#define back_servo_control_pin 10

/*bodyswitch*/
#define yellow_button 4
#define red_button 5
#define servo_button 7
#define music_button 9

/*music*/
SoftwareSerial mySerial(2, 3); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

/*controlerswitch*/
#define right_button 14
#define left_button 15
#define cw_button 16
#define ccw_button 17
#define joint_controler_check 12


//LEDcontrol
#define led_wait_time 75
bool yellow_state = LOW;
bool red_state = LOW;

//servocontrol
#define servo_wait_time 75
Servo myservo_front;
Servo myservo_back;
#define front_stop 88 //1:88 2:92
#define front_left 84 //1:84 2:87
#define front_right 96 //1:96 2:99
#define back_stop 88  //1:88 2:91
#define back_left 95 //1:95 2:97
#define back_right 83 //1:83 2:82

bool servo_state = LOW;

//musiccontrol
#define music_wait_time 75

void setup() {
  mySerial.begin (9600);
  myDFPlayer.begin(mySerial);

  //LED
  pinMode(yellow_control_pin, OUTPUT);
  pinMode(red_control_pin, OUTPUT);

  //bodyswitch
  pinMode(yellow_button, INPUT_PULLUP);
  pinMode(red_button, INPUT_PULLUP);
  pinMode(servo_button, INPUT_PULLUP);
  pinMode(music_button, INPUT_PULLUP);

  //controlerswitch
  pinMode(joint_controler_check, INPUT_PULLUP);
  pinMode(right_button, INPUT_PULLUP);
  pinMode(left_button, INPUT_PULLUP);
  pinMode(cw_button, INPUT_PULLUP);
  pinMode(ccw_button, INPUT_PULLUP);

  //servo
  myservo_front.attach(front_servo_control_pin);
  myservo_back.attach(back_servo_control_pin);
  myservo_front.write(front_stop);
  myservo_back.write(back_stop);
  MsTimer2::set(600, auto_servo_move);
  // MsTimer2::set(600, music);

  //music
//  mySerial.begin (9600);
//  myDFPlayer.begin(mySerial);

  Serial.println(F("DFPlayer Mini online."));
  myDFPlayer.volume(20);  //Set volume value. From 0 to 30
  myDFPlayer.play(1);  //Play the first mp3
}

void loop() {
  control_light(yellow_button, yellow_control_pin, &yellow_state);
  control_light(red_button, red_control_pin, &red_state);
  control_servo();
  control_music();
}




bool manual_flag = LOW;
void control_servo() {
  if (digitalRead(joint_controler_check) == LOW) {
    //Serial.println("manual mode");
    if (manual_flag == LOW) {
      myservo_front.write(front_stop);
      myservo_back.write(back_stop);
      MsTimer2::stop();
      MsTimer2::set(100, manual_servo_move);
      MsTimer2::start();
      manual_flag = HIGH;
    }
  }
  else {
    //Serial.println("auto mode");
    manual_flag = LOW;
    if (digitalRead(servo_button) == LOW & servo_state == LOW) {
      //Serial.println("start");
      MsTimer2::stop();
      MsTimer2::set(600, auto_servo_move);
      MsTimer2::start();
      servo_state = HIGH;
    }
    else if (digitalRead(servo_button) == LOW & servo_state == HIGH) {
      //Serial.println("stop");
      MsTimer2::stop();
      myservo_front.write(front_stop);
      myservo_back.write(back_stop);
      servo_state = LOW;
    }
    delay(servo_wait_time);
  }

}

void manual_servo_move() {
  //Serial.println("timer manual mode");

  if (digitalRead(right_button) == LOW) {
    //Serial.println("right");
    myservo_front.write(front_right);
    myservo_back.write(back_right);
  }
  else if (digitalRead(left_button) == LOW) {
    //Serial.println("left");
    myservo_front.write(front_left);
    myservo_back.write(back_left);
  }
  else if (digitalRead(cw_button) == LOW) {
    //Serial.println("cw");
    myservo_front.write(front_right);
    myservo_back.write(back_left);
  }
  else if (digitalRead(ccw_button) == LOW) {
    //Serial.println("ccw");
    myservo_front.write(front_left);
    myservo_back.write(back_right);
  }
  else {
    //Serial.println("stop");
    myservo_front.write(front_stop);
    myservo_back.write(back_stop);
  }
}

bool cw = LOW;
void auto_servo_move() {
  //Serial.println("timer auto mode");
  if (cw == LOW) {
    myservo_front.write(front_right);
    myservo_back.write(back_right);
    cw = HIGH;
  }
  else {
    myservo_front.write(front_left);
    myservo_back.write(back_left);
    cw = LOW;
  }
}

void control_music() {
  bool switching_music;
  switching_music = digitalRead(music_button);
  if (switching_music == LOW) {
    myDFPlayer.play(2);
  }
  delay(music_wait_time);
}

void control_light(int color_inputpin, int color_outputpin, bool*color) {
  if (digitalRead(color_inputpin) == LOW & *color == LOW) {
    digitalWrite(color_outputpin, HIGH);
    *color = HIGH;
    delay(led_wait_time);
  }

  else if (digitalRead(color_inputpin) == LOW& *color == HIGH) {
    digitalWrite(color_outputpin, LOW);
    *color = LOW;
    delay(led_wait_time);
  }

  else {
    delay(led_wait_time);
  }
}
